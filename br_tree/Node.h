//
// Created by maksim.drobyshev on 10.09.2019.
//

#ifndef AVL_TREE_NODE_H
#define AVL_TREE_NODE_H


template <typename TYPE>
class Node {
private:
    Node<TYPE>* parent;
    signed char colour;
    Node<TYPE>* left;
    Node<TYPE>* right;
    TYPE value;
public:
    explicit Node(Node<TYPE>* parent, TYPE value, signed char colour);

    explicit Node(TYPE value, signed char colour);

    void setColour(signed char _colour);

    signed char getColour();

    Node* getRight();

    Node* getLeft();

    void setRight(Node* node);

    void setLeft(Node* node);

    int getValue();

    void setValue(TYPE value);

    Node<TYPE> *getParent() const;

    void setParent(Node<TYPE> *parent);

    virtual ~Node();
};

#include "Node.tcc"

#endif //AVL_TREE_NODE_H
