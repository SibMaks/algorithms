//
// Created by maksim.drobyshev on 10.09.2019.
//

#ifndef AVL_TREE_TREE_H
#define AVL_TREE_TREE_H


#include "Node.h"

template <typename TYPE>
class Tree {
private:
    Node<TYPE>* root;

    Node<TYPE>* leftRotate(Node<TYPE>* node);

    Node<TYPE>* rightRotate(Node<TYPE>* node);

    static Node<TYPE>* getUncle(Node<TYPE>* node);

    static Node<TYPE>* getSibling(Node<TYPE>* node);

    static Node<TYPE>* getGrandparent(Node<TYPE>* node);

    static unsigned char getColor(Node<TYPE>* node);

    static void setColor(Node<TYPE>* node, unsigned char colour);

    static void insertNode(Node<TYPE>* root, Node<TYPE>* node);

    void doInsertBalance(Node<TYPE>* node);

    void fixAfterRemove(Node<TYPE>* node);

    static void postOrder(Node<TYPE>* node, int indent, bool coloured);

    static void clear(Node<TYPE>* node);

    static TYPE min(Node<TYPE>* root);

    static Node<TYPE>* minNode(Node<TYPE>* root);

    static Node<TYPE>* remove(Node<TYPE>* root, TYPE value);
public:
    Tree();

    void insert(TYPE value);

    void remove(TYPE value);

    void print(bool balanced = false);

    bool contains(TYPE value);

    void clear();

    TYPE min();
};

#include "Tree.tcc"

#endif //AVL_TREE_TREE_H