//
// Created by maksim.drobyshev on 10.09.2019.
//

#include "Node.h"

template<typename TYPE>
void Node<TYPE>::setColour(signed char _colour) {
    this->colour = _colour;
}

template<typename TYPE>
Node<TYPE>* Node<TYPE>::getRight() {
    return right;
}

template<typename TYPE>
Node<TYPE>* Node<TYPE>::getLeft() {
    return left;
}

template<typename TYPE>
void Node<TYPE>::setRight(Node* node) {
    this->right = node;
}

template<typename TYPE>
void Node<TYPE>::setLeft(Node<TYPE>* node) {
    this->left = node;
}

template<typename TYPE>
int Node<TYPE>::getValue() {
    return value;
}

template<typename TYPE>
signed char Node<TYPE>::getColour() {
    return colour;
}

template<typename TYPE>
Node<TYPE>::Node(TYPE value, signed char colour) : Node(nullptr, value, colour) {

}

template<typename TYPE>
Node<TYPE>::Node(Node<TYPE>* parent, TYPE value, signed char colour) : value(value), right(nullptr), left(nullptr),
    colour(colour), parent(parent) {

}

template<typename TYPE>
void Node<TYPE>::setValue(TYPE value) {
    this->value = value;
}

template<typename TYPE>
Node<TYPE> *Node<TYPE>::getParent() const {
    return parent;
}

template<typename TYPE>
void Node<TYPE>::setParent(Node<TYPE> *parent) {
    Node::parent = parent;
}

template<typename TYPE>
Node<TYPE>::~Node() = default;
