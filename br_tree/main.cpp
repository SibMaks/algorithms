#include <iostream>
#include "Tree.h"

void menu() {
    std::cout << "Menu: " << std::endl;
    std::cout << "MENU: for menu print" << std::endl;
    std::cout << "INSERT: for insert val" << std::endl;
    std::cout << "REMOVE: for remove val" << std::endl;
    std::cout << "PRINT: for print tree" << std::endl;
    std::cout << "BPRINT: for print tree with balance factor" << std::endl;
    std::cout << "CLEAR: remove all from tree" << std::endl;
    std::cout << "CONTAINS: is tree contains value or not" << std::endl;
    std::cout << "MIN: find minimum in the tree" << std::endl;
    std::cout << "LOG_ON: after modification print tree" << std::endl;
    std::cout << "LOG_OFF: after modification not print tree" << std::endl;
    std::cout << "EXIT: exit" << std::endl;
}

int main() {
    menu();
    std::string mode;
    auto* tree = new Tree<int>;
    bool log = false;
    do {
        try {
            std::cout << "Input mode: " << std::endl;
            std::cin >> mode;
            if (mode == "MENU") {
                menu();
            } else if (mode == "INSERT") {
                std::cout << "Input value: " << std::endl;
                int val;
                std::cin >> val;
                tree->insert(val);
                if (log) {
                    tree->print(true);
                }
            } else if (mode == "REMOVE") {
                std::cout << "Input value: " << std::endl;
                int val;
                std::cin >> val;
                tree->remove(val);
                if (log) {
                    tree->print(true);
                }
            } else if (mode == "CONTAINS") {
                std::cout << "Input value: " << std::endl;
                int val;
                std::cin >> val;
                std::cout << "Contains: " << (tree->contains(val) ? "yes" : "no") << std::endl;
            } else if (mode == "CLEAR") {
                tree->clear();
                if (log) {
                    tree->print(true);
                }
            } else if (mode == "PRINT") {
                tree->print();
            } else if (mode == "BPRINT") {
                tree->print(true);
            } else if (mode == "MIN") {
                std::cout << "Min is: " << tree->min() << std::endl;
            } else if (mode == "LOG_ON") {
                log = true;
            } else if (mode == "LOG_OFF") {
                log = false;
            }
        } catch (const wchar_t* e) {
            std::wcout << L"Exception: " << e << std::endl;
        }
    } while(mode != "EXIT");
    delete tree;
    return 0;
}