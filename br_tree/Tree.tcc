//
// Created by maksim.drobyshev on 10.09.2019.
//

#include <iostream>
#include <iomanip>
#include "Tree.h"


template<typename TYPE>
Tree<TYPE>::Tree() : root(nullptr) {

}

template<typename TYPE>
void Tree<TYPE>::insert(TYPE value) {
    auto* node = new Node<TYPE>(value, 'R');
    insertNode(root, node);
    doInsertBalance(node);
    root = node;
    while (root != nullptr && root->getParent() != nullptr) {
        root = root->getParent();
    }
}

template<typename TYPE>
void Tree<TYPE>::insertNode(Node<TYPE>* root, Node<TYPE>* node) {
    if (root != nullptr && node->getValue() < root->getValue()) {
        if (root->getLeft() != nullptr) {
            insertNode(root->getLeft(), node);
            return;
        } else {
            root->setLeft(node);
        }
    } else if (root != nullptr) {
        if (root->getRight() != nullptr) {
            insertNode(root->getRight(), node);
            return;
        } else {
            root->setRight(node);
        }
    }
    node->setParent(root);
}

template <typename TYPE>
void Tree<TYPE>::doInsertBalance(Node<TYPE>* node) {
    if (node->getParent() == nullptr) {
        node->setColour('B');
    } else if (node->getParent()->getColour() == 'B') {
        return;
    } else if (getUncle(node) != nullptr && getUncle(node)->getColour() == 'R') {
        node->getParent()->setColour('B');
        getUncle(node)->setColour('B');
        getGrandparent(node)->setColour('R');
        doInsertBalance(getGrandparent(node));
    } else {
        Node<TYPE>* dad = node->getParent();
        Node<TYPE>* grand = getGrandparent(node);

        if (node == dad->getRight() && dad == grand->getLeft()) {
            leftRotate(dad);
            node = node->getLeft();
        } else if (node == dad->getLeft() && dad == grand->getRight()) {
            rightRotate(dad);
            node = node->getRight();
        }

        dad = node->getParent();
        grand = getGrandparent(node);

        if (node == dad->getLeft()) {
            rightRotate(grand);
        } else {
            leftRotate(grand);
        }
        dad->setColour('B');
        grand->setColour('R');
    }
}

template<typename TYPE>
Node<TYPE> *Tree<TYPE>::getUncle(Node<TYPE> *node) {
    Node<TYPE>* dad = node->getParent();
    Node<TYPE>* grand = getGrandparent(node);
    return grand == nullptr ? nullptr : (grand->getLeft() == dad ? grand->getRight() : grand->getLeft());
}

template<typename TYPE>
Node<TYPE>* Tree<TYPE>::leftRotate(Node<TYPE>* node) {
    Node<TYPE>* right = node->getRight();
    Node<TYPE>* dad = node->getParent();

    node->setRight(right->getLeft());
    right->setLeft(node);
    node->setParent(right);

    if (node->getRight() != nullptr) {
        node->getRight()->setParent(node);
    }

    if (dad != nullptr) {
        if (node == dad->getLeft()) {
            dad->setLeft(right);
        } else if (node == dad->getRight()) {
            dad->setRight(right);
        }
    }
    right->setParent(dad);
    return node;
}

template<typename TYPE>
Node<TYPE>* Tree<TYPE>::rightRotate(Node<TYPE>* node) {
    Node<TYPE>* left = node->getLeft();
    Node<TYPE>* dad = node->getParent();

    node->setLeft(left->getRight());
    left->setRight(node);
    node->setParent(left);

    if (node->getLeft() != nullptr) {
        node->getLeft()->setParent(node);
    }

    if (dad != nullptr) {
        if (node == dad->getRight()) {
            dad->setRight(left);
        } else if (node == dad->getLeft()) {
            dad->setLeft(left);
        }
    }
    left->setParent(dad);
    return node;
}

template<typename TYPE>
void Tree<TYPE>::print(bool coloured) {
    postOrder(root, 0, coloured);
}

template<typename TYPE>
void Tree<TYPE>::postOrder(Node<TYPE>* node, int indent, bool coloured) {
    if(node != nullptr) {
        if (node->getRight()) {
            postOrder(node->getRight(), indent + 4, coloured);
        }
        if (indent) {
            std::cout << std::setw(indent) << ' ';
        }
        if (node->getRight()) {
            std::cout << " /" << std::endl << std::setw(indent) << ' ';
        }
        std::stringstream ss;
        if(coloured) {
            ss << "(" << node->getColour() << ")";
        }
        std::cout << node->getValue() << ss.str() << std::endl << " ";
        if (node->getLeft()) {
            std::cout << std::setw(indent) << ' ' << " \\" << std::endl;
            postOrder(node->getLeft(), indent + 4, coloured);
        }
    }
}

template<typename TYPE>
bool Tree<TYPE>::contains(TYPE value) {
    Node<TYPE>* node = root;
    while (node != nullptr && node->getValue() != value) {
        if(node->getValue() > value) {
            node = node->getLeft();
        } else if(node->getValue() < value) {
            node = node->getRight();
        }
    }
    return node != nullptr;
}

template<typename TYPE>
void Tree<TYPE>::clear() {
    clear(root);
    root = nullptr;
}

template<typename TYPE>
void Tree<TYPE>::clear(Node<TYPE>* node) {
    if(node != nullptr) {
        clear(node->getLeft());
        clear(node->getRight());
        delete node;
    }
}

template<typename TYPE>
void Tree<TYPE>::remove(TYPE value) {
    Node<TYPE> *node = remove(root, value);
    fixAfterRemove(node);
}

template<typename TYPE>
Node<TYPE>* Tree<TYPE>::remove(Node<TYPE>* root, TYPE value) {
    if (root == nullptr)
        return root;

    if (value < root->getValue())
        return remove(root->getLeft(), value);

    if (value > root->getValue())
        return remove(root->getRight(), value);

    if (root->getLeft() == nullptr || root->getRight() == nullptr)
        return root;

    Node<TYPE> *temp = minNode(root->getRight());
    root->setValue(temp->getValue());
    return remove(root->getRight(), temp->getValue());
}

template<typename TYPE>
void Tree<TYPE>::fixAfterRemove(Node<TYPE>* node) {

}

template<typename TYPE>
TYPE Tree<TYPE>::min() {
    return min(root);
}

template<typename TYPE>
TYPE Tree<TYPE>::min(Node<TYPE> *root) {
    Node<TYPE> *node = minNode(root);
    return node->getValue();
}

template<typename TYPE>
Node<TYPE> *Tree<TYPE>::getGrandparent(Node<TYPE> *node) {
    return node->getParent() == nullptr ? nullptr : node->getParent()->getParent();
}

template<typename TYPE>
Node<TYPE> *Tree<TYPE>::minNode(Node<TYPE> *root) {
    Node<TYPE> *node = root;
    while (node != nullptr && node->getRight() != nullptr) {
        node = node->getRight();
    }
    if (node == nullptr) {
        throw L"Tree is empty";
    }
    return node;
}

template<typename TYPE>
Node<TYPE> *Tree<TYPE>::getSibling(Node<TYPE> *node) {
    Node<TYPE>* dad = node->getParent();
    if (dad == nullptr) {
        return nullptr;
    }
    if (node == dad->getLeft()) {
        return dad->getRight();
    } else {
        return dad->getLeft();
    }
}

template<typename TYPE>
void Tree<TYPE>::setColor(Node<TYPE> *node, unsigned char colour) {
    if(node != nullptr) {
        node->setColour(colour);
    }
}

template<typename TYPE>
unsigned char Tree<TYPE>::getColor(Node<TYPE> *node) {
    if(node == nullptr) {
        return 'B';
    }
    return node->getColour();
}
