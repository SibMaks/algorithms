//
// Created by maksim.drobyshev on 10.09.2019.
//

#ifndef AVL_TREE_TREE_H
#define AVL_TREE_TREE_H


#include "Node.h"

template <typename TYPE>
class Tree {
private:
    Node<TYPE>* root;

    /**
     * Метод вставки значения в ноду, для рекурсивного использования
     * @param node нода для вставки, должна быть не null
     * @param value значение
     * @return было ли добавлено значение или нет (для балансировки)
     */
    Node<TYPE>* insertInto(Node<TYPE>* node, TYPE value);

    Node<TYPE>* removeFrom(Node<TYPE>* node, TYPE value);

    static Node<TYPE>* leftRotate(Node<TYPE>* node);

    static Node<TYPE>* rightRotate(Node<TYPE>* node);

    static Node<TYPE>* doBalance(Node<TYPE>* node);

    static void postOrder(Node<TYPE>* node, int indent, bool balanced);

    static void clear(Node<TYPE>* node);

    static TYPE min(Node<TYPE>* root);

    static signed char calcBalanceFactor(Node<TYPE> *node);
public:
    Tree();

    void insert(TYPE value);

    void remove(TYPE value);

    void print(bool balanced = false);

    bool contains(TYPE value);

    void clear();

    TYPE min();
};

#include "Tree.tcc"

#endif //AVL_TREE_TREE_H