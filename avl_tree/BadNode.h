//
// Created by maksim.drobyshev on 05.09.2019.
//

#ifndef AVL_TREE_NODE_H
#define AVL_TREE_NODE_H


class BadNode {
private:
    int val;
    unsigned int height;
    BadNode* left;
    BadNode* right;
public:
    BadNode(int _val);

    static unsigned int getHeight(BadNode* node);

    static int calcBalanceFactor(BadNode* node);

    static void doFixHeight(BadNode* node);

    static BadNode* rotateRight(BadNode* node);

    static BadNode* rotateLeft(BadNode* node);

    static BadNode* doBalance(BadNode* node);

    static BadNode* insert(BadNode* node, int val);

    static BadNode* findMin(BadNode* node);

    static BadNode* removeMin(BadNode* node);

    static BadNode* remove(BadNode* node, int val);

    static void postOrder(BadNode *node, int indent);
};


#endif //AVL_TREE_NODE_H
