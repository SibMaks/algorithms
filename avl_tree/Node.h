//
// Created by maksim.drobyshev on 10.09.2019.
//

#ifndef AVL_TREE_NODE_H
#define AVL_TREE_NODE_H


template <typename TYPE>
class Node {
private:
    signed char balanceFactor;
    Node<TYPE>* left;
    Node<TYPE>* right;
    TYPE value;
public:
    explicit Node(TYPE value);

    void setBalanceFactor(signed char _balanceFactor);

    signed char getBalanceFactor();

    Node* getRight();

    Node* getLeft();

    void setRight(Node* node);

    void setLeft(Node* node);

    int getValue();

    void setValue(TYPE value);

    virtual ~Node();
};

#include "Node.tcc"

#endif //AVL_TREE_NODE_H
