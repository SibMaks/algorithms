//
// Created by maksim.drobyshev on 05.09.2019.
//

#include <iostream>
#include <iomanip>
#include "BadNode.h"

BadNode::BadNode(int _val) : val(_val), right(nullptr), left(nullptr), height(1) {

}

unsigned int BadNode::getHeight(BadNode *node) {
    return node == nullptr ? 0 : node->height;
}

int BadNode::calcBalanceFactor(BadNode *node) {
    return getHeight(node->right) - getHeight(node->left);
}

void BadNode::doFixHeight(BadNode *node) {
    unsigned int left = getHeight(node->left);
    unsigned int right = getHeight(node->right);
    node->height = (left > right ? left : right) + 1;
}

BadNode *BadNode::rotateRight(BadNode *node) {
    BadNode* left = node->left;
    node->left = left->right;
    left->right = node;
    doFixHeight(node);
    doFixHeight(left);
    return left;
}

BadNode *BadNode::rotateLeft(BadNode *node) {
    BadNode* right = node->right;
    node->right = right->left;
    right->left= node;
    doFixHeight(node);
    doFixHeight(right);
    return right;
}

BadNode *BadNode::doBalance(BadNode *node) {
    doFixHeight(node);
    int balanceFactor = calcBalanceFactor(node);
    if(balanceFactor == 2) {
        if (calcBalanceFactor(node->right) < 0) {
            node->right = rotateRight(node->right);
        }
        return rotateLeft(node);
    } else if(balanceFactor == -2) {
        if(calcBalanceFactor(node->left) > 0) {
            node->left = rotateLeft(node->left);
        }
        return rotateRight(node);
    }
    return node;
}

BadNode *BadNode::insert(BadNode *node, int val) {
    if(node == nullptr) {
        return new BadNode(val);
    }
    if(val < node->val) {
        node->left = insert(node->left, val);
    } else {
        node->right = insert(node->right, val);
    }
    return doBalance(node);
}

BadNode *BadNode::findMin(BadNode *node) {
    return node->left == nullptr ? node : findMin(node->left);
}

BadNode *BadNode::removeMin(BadNode *node) {
    if(node->left == nullptr) {
        return node->right;
    }
    node->left = removeMin(node->left);
    return doBalance(node);
}

BadNode *BadNode::remove(BadNode *node, int val) {
    if(node == nullptr) {
        return nullptr;
    }
    if(val < node->val) {
        node->left = remove(node->left, val);
    } else if(val > node->val) {
        node->right = remove(node->right, val);
    } else {
        BadNode* left = node->left;
        BadNode* right = node->right;
        delete node;
        if(right == nullptr) {
            return left;
        }
        BadNode* min = findMin(right);
        min->right = removeMin(right);
        min->left = left;
        return doBalance(min);
    }
    return doBalance(node);
}

void BadNode::postOrder(BadNode* node, int indent) {
    if(node != nullptr) {
        if(node->right) {
            postOrder(node->right, indent + 4);
        }
        if (indent) {
            std::cout << std::setw(indent) << ' ';
        }
        if (node->right) {
            std::cout << " /" << std::endl << std::setw(indent) << ' ';
        }
        std::cout << node->val << std::endl << " ";
        if(node->left) {
            std::cout << std::setw(indent) << ' ' <<" \\" << std::endl;
            postOrder(node->left, indent + 4);
        }
    }
}