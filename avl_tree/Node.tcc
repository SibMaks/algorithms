//
// Created by maksim.drobyshev on 10.09.2019.
//

#include "Node.h"

template<typename TYPE>
void Node<TYPE>::setBalanceFactor(signed char _balanceFactor) {
    this->balanceFactor = _balanceFactor;
}

template<typename TYPE>
Node<TYPE>* Node<TYPE>::getRight() {
    return right;
}

template<typename TYPE>
Node<TYPE>* Node<TYPE>::getLeft() {
    return left;
}

template<typename TYPE>
void Node<TYPE>::setRight(Node* node) {
    this->right = node;
}

template<typename TYPE>
void Node<TYPE>::setLeft(Node<TYPE>* node) {
    this->left = node;
}

template<typename TYPE>
int Node<TYPE>::getValue() {
    return value;
}

template<typename TYPE>
signed char Node<TYPE>::getBalanceFactor() {
    return balanceFactor;
}

template<typename TYPE>
Node<TYPE>::Node(TYPE value) : value(value), right(nullptr), left(nullptr), balanceFactor(0) {

}

template<typename TYPE>
void Node<TYPE>::setValue(TYPE value) {
    this->value = value;
}

template<typename TYPE>
Node<TYPE>::~Node() = default;
