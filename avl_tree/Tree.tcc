//
// Created by maksim.drobyshev on 10.09.2019.
//

#include <iostream>
#include <iomanip>

template<typename TYPE>
Tree<TYPE>::Tree() : root(nullptr) {

}

template<typename TYPE>
void Tree<TYPE>::insert(TYPE value) {
    if(root == nullptr) {
        root = new Node<TYPE>(value);
        return;
    }
    root = insertInto(root, value);
}

template <typename TYPE>
signed char Tree<TYPE>::calcBalanceFactor(Node<TYPE>* node) {
    return node == nullptr ? 0 : node->getBalanceFactor();
}

template <typename TYPE>
Node<TYPE>* Tree<TYPE>::doBalance(Node<TYPE>* node) {
    int balanceFactor = calcBalanceFactor(node);
    if(balanceFactor == 2) {
        bool bigRightRotate = calcBalanceFactor(node->getLeft()) < 0;

        Node<TYPE>* left = node->getLeft();
        if(bigRightRotate) {
            Node<TYPE>* leftRight = left->getRight();
            if (leftRight->getBalanceFactor() == 1) {
                node->setBalanceFactor(-1);
                left->setBalanceFactor(0);
            } else if (leftRight->getBalanceFactor() == 0) {
                node->setBalanceFactor(0);
                left->setBalanceFactor(0);
            } else if (leftRight->getBalanceFactor() == -1) {
                node->setBalanceFactor(0);
                left->setBalanceFactor(1);
            }
            leftRight->setBalanceFactor(0);
        } else {
            if (left->getBalanceFactor() == 1) {
                node->setBalanceFactor(0);
                left->setBalanceFactor(0);
            } else if (left->getBalanceFactor() == 0) {
                node->setBalanceFactor(1);
                left->setBalanceFactor(-1);
            }
        }

        if(bigRightRotate) {
            node->setLeft(leftRotate(node->getLeft()));
        }
        return rightRotate(node);
    } else if(balanceFactor == -2) {
        bool bigLeftRotate = calcBalanceFactor(node->getRight()) > 0;

        Node<TYPE>* right = node->getRight();
        if(bigLeftRotate) {
            Node<TYPE>* rightLeft = right->getLeft();
            if (rightLeft->getBalanceFactor() == -1) {
                node->setBalanceFactor(1);
                right->setBalanceFactor(0);
            } else if (rightLeft->getBalanceFactor() == 0) {
                node->setBalanceFactor(0);
                right->setBalanceFactor(0);
            } else if (rightLeft->getBalanceFactor() == 1) {
                node->setBalanceFactor(0);
                right->setBalanceFactor(-1);
            }
            rightLeft->setBalanceFactor(0);
        } else {
            if (right->getBalanceFactor() == -1) {
                node->setBalanceFactor(0);
                right->setBalanceFactor(0);
            } else if (right->getBalanceFactor() == 0) {
                node->setBalanceFactor(-1);
                right->setBalanceFactor(1);
            }
        }

        if (bigLeftRotate) {
            node->setRight(rightRotate(node->getRight()));
        }
        return leftRotate(node);
    }
    return node;
}

template<typename TYPE>
Node<TYPE>* Tree<TYPE>::insertInto(Node<TYPE>* node, TYPE value) {
    if(node == nullptr) {
        return new Node<TYPE>(value);
    }
    if(value < node->getValue()) {
        signed char oldBalanceFactor = node->getLeft() == nullptr ? 42 : node->getLeft()->getBalanceFactor();
        node->setLeft(insertInto(node->getLeft(), value));
        signed char balanceFactor = node->getLeft()->getBalanceFactor();
        if(oldBalanceFactor == 42 || (balanceFactor != oldBalanceFactor && balanceFactor != 0)) {
            node->setBalanceFactor(node->getBalanceFactor() + 1);
        }
    } else if(value > node->getValue()) {
        signed char oldBalanceFactor = node->getRight() == nullptr ? 42 : node->getRight()->getBalanceFactor();
        node->setRight(insertInto(node->getRight(), value));
        signed char balanceFactor = node->getRight()->getBalanceFactor();
        if(oldBalanceFactor == 42 || (balanceFactor != oldBalanceFactor && balanceFactor != 0)) {
            node->setBalanceFactor(node->getBalanceFactor() - 1);
        }
    }
    return doBalance(node);
}

template<typename TYPE>
Node<TYPE>* Tree<TYPE>::leftRotate(Node<TYPE>* node) {
    Node<TYPE>* right = node->getRight();
    node->setRight(right->getLeft());
    right->setLeft(node);
    return right;
}

template<typename TYPE>
Node<TYPE>* Tree<TYPE>::rightRotate(Node<TYPE>* node) {
    Node<TYPE>* left = node->getLeft();
    node->setLeft(left->getRight());
    left->setRight(node);
    return left;
}

template<typename TYPE>
void Tree<TYPE>::print(bool balanced) {
    postOrder(root, 0, balanced);
}

template<typename TYPE>
void Tree<TYPE>::postOrder(Node<TYPE>* node, int indent, bool balanced) {
    if(node != nullptr) {
        if (node->getRight()) {
            postOrder(node->getRight(), indent + 4, balanced);
        }
        if (indent) {
            std::cout << std::setw(indent) << ' ';
        }
        if (node->getRight()) {
            std::cout << " /" << std::endl << std::setw(indent) << ' ';
        }
        std::stringstream ss;
        if(balanced) {
            ss << "(" << (int)node->getBalanceFactor() << ")";
        }
        std::cout << node->getValue() << ss.str() << std::endl << " ";
        if (node->getLeft()) {
            std::cout << std::setw(indent) << ' ' << " \\" << std::endl;
            postOrder(node->getLeft(), indent + 4, balanced);
        }
    }
}

template<typename TYPE>
bool Tree<TYPE>::contains(TYPE value) {
    Node<TYPE>* node = root;
    while (node != nullptr && node->getValue() != value) {
        if(node->getValue() > value) {
            node = node->getLeft();
        } else if(node->getValue() < value) {
            node = node->getRight();
        }
    }
    return node != nullptr;
}

template<typename TYPE>
void Tree<TYPE>::clear() {
    clear(root);
    root = nullptr;
}

template<typename TYPE>
void Tree<TYPE>::clear(Node<TYPE>* node) {
    if(node != nullptr) {
        clear(node->getLeft());
        clear(node->getRight());
        delete node;
    }
}

template<typename TYPE>
void Tree<TYPE>::remove(TYPE value) {
    if(root == nullptr) {
        return;
    }
    if(contains(value)) {
        root = removeFrom(root, value);
    } else {
        throw L"Value not found";
    }
}

template<typename TYPE>
Node<TYPE>* Tree<TYPE>::removeFrom(Node<TYPE>* node, TYPE value) {
    if (value < node->getValue()) {
        node->setLeft(removeFrom(node->getLeft(), value));
        if(node->getLeft() == nullptr || node->getLeft()->getBalanceFactor() == 0) {
            node->setBalanceFactor(node->getBalanceFactor() - 1);
        }
    } else if (value > node->getValue()) {
        node->setRight(removeFrom(node->getRight(), value));
        if(node->getRight() == nullptr || node->getRight()->getBalanceFactor() == 0) {
            node->setBalanceFactor(node->getBalanceFactor() + 1);
        }
    } else {
        if(node->getLeft() == nullptr || node->getRight() == nullptr) {
            Node<TYPE>* newNode = node->getLeft() == nullptr ? node->getRight() : node->getLeft();
            delete node;
            return newNode;
        } else {
            int minVal = min(node->getRight());
            node->setRight(removeFrom(node->getRight(), minVal));
            node->setValue(minVal);
            if(node->getRight() == nullptr || node->getRight()->getBalanceFactor() == 0) {
                node->setBalanceFactor(node->getBalanceFactor() + 1);
            }
        }
    }
    return doBalance(node);
}

template<typename TYPE>
TYPE Tree<TYPE>::min() {
    Node<TYPE> *node = root;
    while (node != nullptr && node->getRight() != nullptr) {
        node = node->getRight();
    }
    if (node == nullptr) {
        throw L"Tree is empty";
    }
    return node->getValue();
}

template<typename TYPE>
TYPE Tree<TYPE>::min(Node<TYPE> *root) {
    Node<TYPE> *node = root;
    while (node != nullptr && node->getLeft() != nullptr) {
        node = node->getLeft();
    }
    if (node == nullptr) {
        throw L"Node is empty";
    }
    return node->getValue();
}
